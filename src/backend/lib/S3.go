package lib

import (
	"bytes"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

// Data is a datastruecture
type Data struct {
	Label   string
	File    string
	BaseKey string
}

// Config is aws S3 configure
type Config struct {
	Aws struct {
		S3 struct {
			Region          string
			Bucket          string
			AccessKeyID     string
			SecretAccessKey string
		}
	}
}

// Dataset is a datastructure
type Dataset struct {
	Classes map[string][]Data
}

func newConfig() *Config {
	c := new(Config)

	c.Aws.S3.Region = "us-east-1"
	c.Aws.S3.Bucket = "jatapplication"
	c.Aws.S3.AccessKeyID = "AKIASMP2OFIRB6RH27V3"
	c.Aws.S3.SecretAccessKey = "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"

	return c
}

func createsess(config *Config) *session.Session {
	sess := session.Must(session.NewSession(&aws.Config{
		Credentials: credentials.NewStaticCredentials(config.Aws.S3.AccessKeyID, config.Aws.S3.SecretAccessKey, ""),
		Region:      aws.String(config.Aws.S3.Region),
	}))
	return sess
}

// GetS3Data get S3 dataset
func GetS3Data(userid string) (string, string, string) {
	config := newConfig()
	sess := createsess(config)
	svc := s3.New(sess)
	res, _ := svc.ListObjects(&s3.ListObjectsInput{Bucket: aws.String(config.Aws.S3.Bucket)})
	iconPath := "default/icon.png"
	headerPath := "default/header.png"
	var ext string
	for _, item := range res.Contents {
		_, basename := filepath.Split(*item.Key)
		ext = filepath.Ext(basename)
		if "icon/"+userid+ext == *item.Key {
			iconPath = "icon/" + userid + ext
		}
		if "header/"+userid+ext == *item.Key {
			headerPath = "header/" + userid + ext
		}
	}
	fmt.Println(iconPath)
	iconbase64 := getImageBase64(iconPath, config, svc)
	headerbase64 := getImageBase64(headerPath, config, svc)
	return iconbase64, headerbase64, ext
}

func getImageBase64(path string, config *Config, svc *s3.S3) string {
	data, _ := svc.GetObject(&s3.GetObjectInput{
		Bucket: aws.String(config.Aws.S3.Bucket),
		Key:    aws.String(path),
	})
	buf := new(bytes.Buffer)
	io.Copy(buf, data.Body)
	return base64.StdEncoding.EncodeToString(buf.Bytes())
}

// Upload is S3 upload functon
func Upload(b64, filename, ext, dist string) error {
	format, exterr := checkformat(ext)
	if exterr != nil {
		return exterr
	}

	config := newConfig()
	sess := createsess(config)
	uploader := s3manager.NewUploader(sess)

	data, _ := base64.StdEncoding.DecodeString(b64)
	wb := new(bytes.Buffer)
	wb.Write(data)
	// upload s3
	_, err := uploader.Upload(&s3manager.UploadInput{
		Body:        wb,
		Bucket:      aws.String(config.Aws.S3.Bucket),
		ContentType: aws.String(format),
		Key:         aws.String(dist),
	})
	if err != nil {
		return err
	}
	return nil
}

func checkname(name string) bool {
	if name != "" {
		return false
	}
	return true
}

func checkformat(ext string) (string, error) {
	var contentType string
	switch ext {
	case "jpg":
		contentType = "image/jpeg"
	case "jpeg":
		contentType = "image/jpeg"
	case "gif":
		contentType = "image/gif"
	case "png":
		contentType = "image/png"
	default:
		return "", errors.New("this extension is invalid")
	}
	return contentType, nil
}

// DeleteData delete execution
func DeleteData(key string) bool {
	config := newConfig()
	sess := createsess(config)
	svc := s3.New(sess)
	_, err := svc.DeleteObject(&s3.DeleteObjectInput{Bucket: aws.String(config.Aws.S3.Bucket), Key: aws.String(key)})
	if err != nil {
		return false
	}
	err = svc.WaitUntilObjectNotExists(&s3.HeadObjectInput{
		Bucket: aws.String(config.Aws.S3.Bucket),
		Key:    aws.String(key),
	})
	if err != nil {
		return false
	}
	return true
}

// DeleteDatas delete folder
func DeleteDatas(userid, did string) {
	config := newConfig()
	sess := createsess(config)
	svc := s3.New(sess)
	res, _ := svc.ListObjects(&s3.ListObjectsInput{Bucket: aws.String(config.Aws.S3.Bucket)})
	// userid/データセットID/class名/画像名
	for _, item := range res.Contents {
		basekey := *item.Key
		key := strings.Replace(basekey, " ", "", -1)
		key = strings.Replace(basekey, "/", " ", -1)
		reg := regexp.MustCompile(`\S+`)
		elm := reg.FindAllString(key, -1)
		ID := elm[0]
		datasetid := elm[1]
		if ID == userid && datasetid == did {
			DeleteData(basekey)
		}
	}
}
