package lib

import (
	"database/sql"
	"log"

	// qp is postgres driver
	_ "github.com/lib/pq"
)

// Connect connect Postgres SQL DB server
func Connect() *sql.DB {
	user := "lkeix"
	password := "dummy"
	dbname := "jat"
	db, err := sql.Open("postgres", "host=127.0.0.1 port=5432 user="+user+" password="+password+" dbname="+dbname+" sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	return db
}
