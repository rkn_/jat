package lib

import (
	"fmt"
	"time"
)

// Opponent is data structure returning user opponent
type Opponent struct {
	OpponentID string
	Name       string
	Icon       string
	Ext        string
}

// Chat is data structure user chat data.
type Chat struct {
	UserID  string
	Content string
	Date    string
	Icon    string
	Ext     string
}

// ReadLoginPassword read user login password from user id, return password hash, get or not.
func ReadLoginPassword(userid string) (string, bool) {
	query := "select password from users where userid = $1"
	args := []interface{}{
		userid,
	}
	rows, _ := GetRow(query, args)
	if len(rows) <= 0 {
		return "", false
	}
	return rows[0][0].(string), true
}

// ReadLoginPasswordfromMail read user login password from user id, return password hash, get or not.
func ReadLoginPasswordfromMail(mail string) (string, bool) {
	query := "select password from users where mail = $1"
	args := []interface{}{
		mail,
	}
	rows, _ := GetRow(query, args)
	if len(rows) <= 0 {
		return "", false
	}
	return rows[0][0].(string), true
}

// InsertLoginUserData insert userid, password hash, mail addr, return executed or not.
func InsertLoginUserData(userid, password, mail string) bool {
	query := "insert into users (userid, password, mail) values ($1, $2, $3)"
	args := []interface{}{
		userid, password, mail,
	}
	return Exec(query, args)
}

// InitProfile is called signup
func InitProfile(userid string) {
	query := "insert into profile (userid, name, link, bio) values ($1, $2, $3, $4)"
	args := []interface{}{
		userid, userid, "", "",
	}
	Exec(query, args)
}

// ReadProfile read user profile data (result[0] = userid, result[1] = name, result[2] = bio, result[3] = link)
func ReadProfile(userid string) []interface{} {
	query := "select userid, name, bio, link from profile where userid = $1"
	args := []interface{}{
		userid,
	}
	rows, _ := GetRow(query, args)
	if len(rows) <= 0 {
		return nil
	}
	return rows[0]
}

// ReadUserID read user data from DB
func ReadUserID(id string) [][]interface{} {
	id = id + "%"
	query := "select users.userid, profile.name from users, profile where users.userid LIKE $1 and users.userid = profile.userid"
	args := []interface{}{
		id,
	}
	rows, _ := GetRow(query, args)
	return rows
}

// InsertChat insert chat data
func InsertChat(content, opponent, id, date string) int64 {
	query := "select roomid from relation where (opponentid = $1 and userid = $2) or (opponentid = $2 and userid = $1)"
	args := []interface{}{
		opponent, id,
	}
	rows, _ := GetRow(query, args)
	var roomid int64
	if len(rows) != 0 {
		roomid = rows[0][0].(int64)
	} else {
		query = "insert into relation (opponentid, userid) values ($1, $2) RETURNING roomid"
		rows, _ := GetRow(query, args)
		roomid = rows[0][0].(int64)
	}
	query = "insert into chat (date, content, roomid, userid) values ($1, $2, $3, $4)"
	now := time.Now()
	args = []interface{}{
		now, content, roomid, id,
	}
	Exec(query, args)
	return roomid
}

// ReadRelation read relation user data
func ReadRelation(userid string) []Opponent {
	query := "select profile.userid, relation.opponentid, profile.name from relation, profile where ($1 = relation.opponentid or $1 = relation.userid) and (profile.userid = relation.opponentid or profile.userid = relation.userid) and (profile.userid != $1)"
	args := []interface{}{
		userid,
	}
	rows, _ := GetRow(query, args)
	fmt.Println(rows)
	var res []Opponent
	if len(rows) != 0 {
		var tmp Opponent
		for i := 0; i < len(rows); i++ {
			tmp.OpponentID = rows[i][0].(string)
			if rows[i][1].(string) != userid {
				tmp.OpponentID = rows[i][1].(string)
			}
			tmp.Name = rows[i][2].(string)
			res = append(res, tmp)
		}
	}
	return res
}

// ReadChat read chat data
func ReadChat(opponentid, userid string) []Chat {
	query := "select chat.date, chat.content, chat.userid from chat where chat.roomid in (select relation.roomid from relation where (relation.userid = $1 and relation.opponentid = $2) or (relation.userid = $2 and relation.opponentid = $1))"
	args := []interface{}{
		userid, opponentid,
	}
	rows, _ := GetRow(query, args)
	var res []Chat
	if len(rows) > 0 {
		for i := 0; i < len(rows); i++ {
			var tmp Chat
			d := rows[i][0].(time.Time)
			tmp.Date = d.Format("2006/1/2 15:04")
			tmp.Content = rows[i][1].(string)
			tmp.UserID = rows[i][2].(string)
			res = append(res, tmp)
		}
	}
	return res
}

// ReadRoomID read chat room id
func ReadRoomID(userid, opponentid string) int64 {
	query := "select roomid from relation where (userid = $1 and opponentid = $2) or (userid = $2 and opponentid = $1)"
	args := []interface{}{
		userid, opponentid,
	}
	rows, _ := GetRow(query, args)
	if len(rows) > 0 {
		return rows[0][0].(int64)
	}
	return -1
}

// UpdateProfile update user profile
func UpdateProfile(userid, name, bio, link string) {
	query := "update profile set name = $1, bio = $2, link = $3 where userid = $4"
	args := []interface{}{
		name, bio, link, userid,
	}
	Exec(query, args)
}
