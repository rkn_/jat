package middleware

import (
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// AuthErr is authorized error
type AuthErr struct {
	Login   bool
	Message string
}

// Middleware define logger, authorization function
func Middleware(typ string) gin.HandlerFunc {
	if typ == "auth" {
		return auth
	} else if typ == "logger" {
		return logger
	} else {
		return nil
	}
}

func logger(ctx *gin.Context) {
	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatal(err.Error())
	}
	oldTime := time.Now()
	ua := ctx.GetHeader("User-Agent")
	ctx.Next()
	logger.Info("incoming request",
		zap.String("path", ctx.Request.URL.Path),
		zap.String("Ua", ua),
		zap.Int("status", ctx.Writer.Status()),
		zap.Duration("elapsed", time.Now().Sub(oldTime)),
	)
}

func auth(ctx *gin.Context) {
	session := sessions.Default(ctx)
	id := session.Get("userid")
	login := session.Get("login")
	if id != nil && login != nil {
		idStr := id.(string)
		login := login.(bool)
		if len(idStr) >= 3 && login {
			ctx.Next()
		}
	} else {
		// ctx.JSON(401, gin.H{"message": "you don't have authorization token"})
		/*
			var res AuthErr
			res.Login = false
			res.Message = "認証トークンがありません。"
			bytes, _ := json.Marshal(res)
			ctx.Writer.Write(bytes)
		*/
		ctx.Abort()
		ctx.Redirect(http.StatusMovedPermanently, "/login")
	}
}
