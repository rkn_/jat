package router

import (
	"encoding/json"
	"fmt"
	"jat/src/backend/api"
	"jat/src/backend/lib"
	"jat/src/backend/middleware"
	"log"
	"os"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
	"gopkg.in/olahol/melody.v1"
)

var m = melody.New()
var sess []*melody.Session

// CreateRouter return api router
func CreateRouter(router *gin.Engine) *gin.Engine {
	prjpath := os.Getenv("GOPATH") + "/src/jat/"
	// storeStr := os.Getenv("APPNAME_SESSION_KEY")
	// fmt.Println(storeStr)
	// storeplace := []byte(storeStr)
	store := sessions.NewCookieStore([]byte("secret"))
	router.Use(sessions.Sessions("session", store))
	// router.Use(middleware.Middleware("logger"))

	router.LoadHTMLFiles(prjpath + "dist/index.html")
	router.Static("/css", prjpath+"dist/css")
	router.Static("/js", prjpath+"dist/js")
	router.Static("/fonts", prjpath+"dist/fonts")

	open := router.Group("")
	open.Use(middleware.Middleware("auth"))
	open.GET("/talk", view)
	open.GET("/home", view)
	router.GET("/login", view)

	router.POST("/authapi/login", api.Login)
	router.POST("/authapi/signup", api.Signup)
	router.POST("/logout", api.Logout)
	router.POST("/api/getAuth", api.GetAuth)
	router.POST("/api/getUserid", api.GetUserID)
	group := router.Group("/api")
	group.Use(middleware.Middleware("auth"))
	{
		group.POST("/updateProfile", api.UpdateProfile)
		group.POST("/readProfile", api.ReadProfile)
		group.POST("/readUser", api.ReadUsers)
		group.POST("/readOpponents", api.ReadOpponents)
		group.POST("/readChat", api.ReadChat)
		group.POST("/updateProf", api.UpdateProfile)
		group.GET("/chatws", func(ctx *gin.Context) {
			m.HandleRequest(ctx.Writer, ctx.Request)
		})

		m.HandleMessage(func(s *melody.Session, msg []byte) {
			var data map[string]string
			json.Unmarshal(msg, &data)
			if dtype, _ := data["type"]; dtype == "open" {
				opponentid := data["opponentid"]
				userid := data["userid"]
				roomid := lib.ReadRoomID(userid, opponentid)
				s.Set("roomid", roomid)
				sess = append(sess, s)
			}
			if dtype, _ := data["type"]; dtype == "chat" {
				var content string
				var opponent string
				var userid string
				var date string
				var ok bool
				if content, ok = data["content"]; !ok {
					fmt.Println("a")
					return
				}
				if opponent, ok = data["opponentid"]; !ok {
					fmt.Println("b")
					return
				}
				if userid, ok = data["id"]; !ok {
					fmt.Println("c")
					return
				}
				if date, ok = data["date"]; !ok {
					fmt.Println("d")
					return
				}
				lib.InsertChat(content, opponent, userid, date)
				// m.Broadcast(msg)
				var tmp []*melody.Session
				tmproomid, _ := s.Get("roomid")
				for _, session := range sess {
					if val, _ := session.Get("roomid"); val.(int64) == tmproomid.(int64) {
						tmp = append(tmp, session)
					}
				}
				m.BroadcastMultiple(msg, tmp)
				/*
					m.BroadcastFilter(msg, func(s *melody.Session) bool {
						val, ext := s.Get("roomid")
						if !ext {
							return false
						}
						if lib.ReadRoomID(userid, opponent) == val.(int64) {
							return true
						}
						return false
					})
				*/
			}
		})

		m.HandleConnect(func(s *melody.Session) {
			log.Printf("websocket connection open. [session: %#v]\n", s)
		})

		m.HandleDisconnect(func(s *melody.Session) {
			for key, session := range sess {
				if session == s {
					sess = delete(sess, key)
				}
			}
		})
	}
	return router
}

func delete(s []*melody.Session, i int) []*melody.Session {
	s = append(s[:i], s[i+1:]...)
	n := make([]*melody.Session, len(s))
	copy(n, s)
	return n
}

func chat(ctx *gin.Context) {
	m.HandleRequest(ctx.Writer, ctx.Request)
}

func view(ctx *gin.Context) {
	ctx.HTML(200, "index.html", gin.H{})
}
