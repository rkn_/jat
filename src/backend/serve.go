package main

import (
	"jat/src/backend/router"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r = router.CreateRouter(r)
	r.Run(":8081")
}
