package api

import (
	"encoding/json"
	"fmt"
	"jat/src/backend/lib"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
)

// Profile is profile user data.
type Profile struct {
	UserID string
	Name   string
	Bio    string
	Header string
	Icon   string
	Link   string
}

// ProfStrData is data structure
type ProfStrData struct {
	Bio  string
	Link string
	Name string
}

// UpdateProfile update user profile
func UpdateProfile(ctx *gin.Context) {
	session := sessions.Default(ctx)
	userid := session.Get("userid").(string)
	req := ctx.Request
	req.ParseForm()
	profileJSONStr := req.PostFormValue("data")
	var prof map[string]string
	json.Unmarshal(([]byte)(profileJSONStr), &prof)
	lib.UpdateProfile(userid, prof["Name"], prof["Bio"], prof["Link"])
	header := req.PostFormValue("header")
	icon := req.PostFormValue("icon")
	iconext := req.PostFormValue("iconext")
	headerext := req.PostFormValue("headerext")
	fmt.Println(lib.Upload(icon, userid, iconext, "icon/"+userid+"."+iconext))
	lib.Upload(header, userid, headerext, "header/"+userid+"."+headerext)
}

// ReadProfile read user profile
func ReadProfile(ctx *gin.Context) {
	session := sessions.Default(ctx)
	idInterface := session.Get("userid")
	var res Profile
	if idInterface != nil {
		userid := idInterface.(string)
		var ext string
		res.Icon, res.Header, ext = lib.GetS3Data(userid)
		res.Icon = "data:image/" + ext + ";base64," + res.Icon
		res.Header = "data:image/" + ext + ";base64," + res.Header
		data := lib.ReadProfile(userid)
		if data != nil {
			res.UserID = data[0].(string)
			res.Name = data[1].(string)
			res.Bio = data[2].(string)
			res.Link = data[3].(string)
		}
	}
	jsonStr, _ := json.Marshal(res)
	ctx.Writer.Write(jsonStr)
}
