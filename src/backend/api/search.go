package api

import (
	"encoding/json"
	"jat/src/backend/lib"

	"github.com/gin-gonic/gin"
)

// User is user data structure
type User struct {
	ID   string
	Name string
	Icon string
	Ext  string
}

// ReadUsers read user from user input
func ReadUsers(ctx *gin.Context) {
	req := ctx.Request
	req.ParseForm()
	opponent := req.PostFormValue("opponent")
	row := lib.ReadUserID(opponent)
	var res []User

	if len(row) >= 0 {
		for i := 0; i < len(row); i++ {
			var user User
			user.ID = row[i][0].(string)
			user.Name = row[i][1].(string)
			icon, _, ext := lib.GetS3Data(user.ID)
			user.Icon = icon
			user.Ext = ext[1:]
			res = append(res, user)
		}
	}
	jsonStr, _ := json.Marshal(res)
	ctx.Writer.Write(jsonStr)
}
