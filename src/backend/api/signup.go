package api

import (
	"jat/src/backend/lib"
	"jat/src/backend/utils"

	"github.com/gin-gonic/contrib/sessions"

	"github.com/gin-gonic/gin"
)

// Signup Handler is called user signup, require ID, password, mail
func Signup(ctx *gin.Context) {
	session := sessions.Default(ctx)
	req := ctx.Request
	req.ParseForm()
	userid := req.PostFormValue("userid")
	password := req.PostFormValue("password")
	mail := req.PostFormValue("mail")
	if varifyUserid(userid) && varifyUsermail(mail) {
		hashed := utils.CryptPasswd(password)
		lib.InsertLoginUserData(userid, hashed, mail)
		lib.InitProfile(userid)
		session.Set("login", true)
		session.Set("userid", userid)
		session.Save()
		ctx.JSON(200, gin.H{"message": "登録が完了しました。", "result": true})
	} else {
		ctx.JSON(401, gin.H{"message": "入力されたIDもしくは、メールはすでに登録されています。", "result": false})
	}
}

func varifyUserid(userid string) bool {
	_, res := lib.ReadLoginPassword(userid)
	return !res
}

func varifyUsermail(mail string) bool {
	_, res := lib.ReadLoginPasswordfromMail(mail)
	return !res
}
