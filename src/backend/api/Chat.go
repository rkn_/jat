package api

import (
	"encoding/json"
	"jat/src/backend/lib"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
)

// ReadChat read user chats data
func ReadChat(ctx *gin.Context) {
	session := sessions.Default(ctx)
	req := ctx.Request
	req.ParseForm()
	opponentid := req.PostFormValue("opponentid")
	userid := session.Get("userid").(string)
	res := lib.ReadChat(opponentid, userid)
	jsonStr, _ := json.Marshal(res)
	ctx.Writer.Write(jsonStr)
}

// InsertChat insert user chat data
func InsertChat(ctx *gin.Context) {

}
