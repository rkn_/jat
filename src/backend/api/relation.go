package api

import (
	"encoding/json"
	"jat/src/backend/lib"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
)

// ReadOpponents is end point to get relation
func ReadOpponents(ctx *gin.Context) {
	session := sessions.Default(ctx)
	if userid := session.Get("userid"); userid != nil {
		res := lib.ReadRelation(userid.(string))
		for i := 0; i < len(res); i++ {
			icon, _, ext := lib.GetS3Data(res[i].OpponentID)
			res[i].Icon = icon
			res[i].Ext = ext[1:]
		}
		jsonStr, _ := json.Marshal(res)
		ctx.Writer.Write(jsonStr)
	}
}
