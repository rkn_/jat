package api

import (
	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
)

// GetAuth get login
func GetAuth(ctx *gin.Context) {
	session := sessions.Default(ctx)
	if login := session.Get("login"); login != nil {
		if login.(bool) {
			ctx.Writer.WriteString("true")
		} else {
			ctx.Writer.WriteString("false")
		}
	}
}

// GetUserID get id
func GetUserID(ctx *gin.Context) {
	session := sessions.Default(ctx)
	if userid := session.Get("userid"); userid != nil {
		ctx.Writer.WriteString(userid.(string))
	}
}
