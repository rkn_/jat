package api

import (
	"jat/src/backend/lib"
	"jat/src/backend/utils"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
)

// Login is processing login api api
func Login(ctx *gin.Context) {
	session := sessions.Default(ctx)
	req := ctx.Request
	req.ParseForm()
	userid := req.PostFormValue("userid")
	password := req.PostFormValue("password")
	hash, ok := lib.ReadLoginPassword(userid)
	if !ok {
		ctx.JSON(403, gin.H{"message": "IDもしくはパスワードが間違っています", "result": false})
	}
	if utils.ComparePasswd(password, hash) {
		session.Set("login", true)
		session.Set("userid", userid)
		session.Save()
		ctx.JSON(200, gin.H{"message": "ログインに成功しました", "result": true})
	} else {
		ctx.JSON(403, gin.H{"message": "IDもしくはパスワードが間違っています", "result": false})
	}
}
