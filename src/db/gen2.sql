CREATE TABLE users
(
  userid VARCHAR(30) NOT NULL,
  password VARCHAR(256) NOT NULL,
  mail VARCHAR(64) NOT NULL,
  PRIMARY KEY (userid)
);

CREATE TABLE profile
(
  name VARCHAR(30) NOT NULL,
  bio VARCHAR(200) NOT NULL,
  link VARCHAR(512) NOT NULL,
  userid VARCHAR(30) NOT NULL,
  FOREIGN KEY (userid) REFERENCES users(userid) on delete cascade on update cascade
);

CREATE TABLE relation
(
  roomid serial NOT NULL,
  opponentid VARCHAR(30) NOT NULL,
  userid VARCHAR(30) NOT NULL,
  PRIMARY KEY (roomid),
  FOREIGN KEY (opponentid) REFERENCES users(userid) on delete cascade on update cascade,
  FOREIGN KEY (userid) REFERENCES users(userid) on delete cascade on update cascade
);

CREATE TABLE chat
(
  date date NOT NULL,
  content VARCHAR(1024) NOT NULL,
  roomid serial NOT NULL,
  FOREIGN KEY (roomid) REFERENCES relation(roomid) on delete cascade on update cascade
);