import axios from 'axios'

export default function post(link, params) {
    return axios.post(link, params)
}