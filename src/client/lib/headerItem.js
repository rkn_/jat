export default function toolBarItem(login) {
  let res = {}
  if (login) {
    res = [
      {
        icon: 'mdi-home',
        label: 'Home',
        link: 'home'
      },
      {
        icon: 'mdi-message',
        label: 'talk',
        link: 'talk'
      },
      {
        icon: 'mdi-account-minus',
        label: 'logout',
        link: 'logout'
      }
    ]
  } else {
    res = [
      {
        icon: 'mdi-account-check',
        label: 'Login',
        link: 'login'
      }
    ]
  }
  return res
}