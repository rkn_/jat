import Vue from 'vue'
import VueRouter from 'vue-router'
import login from '@/client/views/login.vue'
import home from '@/client/views/home.vue'
import talk from '@/client/views/talk.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'Login',
    meta: {
      isPublic: true
    },
    component: login
  },
  {
    path: '/home',
    name: 'Home',
    component: home
  },
  {
    path: '/talk',
    name: 'Talk',
    component: talk
  }
]

const router = new VueRouter({
  scrollBehavior: async (to, from, savedPosition) => {
    if (savedPosition) {
      return savedPosition;
    }

    const findEl = async (hash, x) => {
      return document.querySelector(hash) ||
        new Promise((resolve, reject) => {
          console.log(reject)
          if (x > 50) {
            return resolve();
          }
          setTimeout(() => {
            resolve(findEl(hash, ++x || 1));
          }, 100);
        });
    }
    if (to.hash) {
      const el = await findEl(to.hash);
      if ('scrollBehavior' in document.documentElement.style) {
        return window.scrollTo({ top: el.offsetTop, behavior: 'smooth' });
      } else {
        return window.scrollTo(0, el.offsetTop);
      }
    }
    return { x: 0, y: 0 };
  },
  base: process.env.BASE_URL,
  routes,
  mode: 'history'
})

export default router
