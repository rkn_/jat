import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router'
// import post from '@/client/lib/post.js'
import store from './store'

Vue.config.productionTip = false

/*
router.beforeEach((to, _, next) => {
  post('/api', {}).then(() => {
      if (to.matched.some(record => !record.meta.isPublic)) {
          next('/login')
      } else {
          next()
      }
  }).catch(() => {
    next('/login')
  })
})
*/


new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
