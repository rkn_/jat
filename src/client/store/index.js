import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

const store = {
    state: {
        bar: {
            height: ''
        },
        auth: {
            id: ''
        }
    },
    mutations: {
        setH(state, height) {
            state.bar.height = height
        },
        setID(state, id) {
            state.auth.id = id
        },
        logout(state) {
            state.auth = {
                id: ''
            }
        }
    },
    getters: {
        getBarH(state) {
            return state.bar.height
        },
        getID(state) {
            return state.auth.id
        }
    },
    plugins: [
        createPersistedState({ storage: window.sessionStorage })
    ]
}

export default new Vuex.Store(store)