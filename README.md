# jat

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


## Postgres
```
postgres -D /usr/local/var/postgres

psql -f ./src/db/gen2.sql -U lkeix -d jat

```