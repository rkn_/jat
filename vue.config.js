module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  pages: {
    top: {
      entry: 'src/client/main.js',
      template: 'public/index.html',
      filename: 'index.html'
    }
  }
}